# Home automation dry-run

This repo logs some experiments made on a raspberry-pi zero w v1.1 with an
enviro+ hat.

Docker composer with:

- Home assistant
- mosquitto
- Kotori

Stuff done on the raspberry-pi:

- Install the enviro software ([enviro github][]):
  `curl -sSL https://get.pimoroni.com/enviroplus | bash`

- Add the user service under rpi/send-mqtt-mimir.service:
  `systemctl --user edit --full --force send-mqtt-mimir`

Mind that there's a bunch of hardcoded stuff on that systemd-service.

[enviro github]: https://github.com/pimoroni/enviroplus-python/
